import * as cdk from 'aws-cdk-lib';
import * as s3 from "aws-cdk-lib/aws-s3"
import { Construct } from 'constructs';
// import * as sqs from 'aws-cdk-lib/aws-sqs';

export class MyCdkS3ProjectStack extends cdk.Stack {
  constructor(scope: Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

    // The code that defines your stack goes here
    
    // Use AWS CDK to create an S3 bucket with versioning and encryption enabled
    new s3.Bucket(this, 'MyBucket', {
      // Enable versioning
      versioned: true,

      // Block public access
      blockPublicAccess: s3.BlockPublicAccess.BLOCK_ALL,

      // Enable server-side encryption with an AWS managed key
      encryption: s3.BucketEncryption.KMS_MANAGED,

      // Optional: Enforce SSL for data in transit
      enforceSSL: true,

      // Optional: Enable bucket lifecycle rules
      lifecycleRules: [
        {
          transitions: [
            {
              // Transition to STANDARD_IA (infrequent access) after 30 days
              storageClass: s3.StorageClass.INFREQUENT_ACCESS,
              transitionAfter: cdk.Duration.days(30),
            },
            {
              // Archive to GLACIER after 1 year
              storageClass: s3.StorageClass.GLACIER,
              transitionAfter: cdk.Duration.days(365),
            },
          ],
          expiration: cdk.Duration.days(3650), // Expire after 10 years
        },
      ],
    });

    // example resource
    // const queue = new sqs.Queue(this, 'MyCdkS3ProjectQueue', {
    //   visibilityTimeout: cdk.Duration.seconds(300)
    // });
  }
}
