import * as cdk from 'aws-cdk-lib';
import { Construct } from 'constructs';
export declare class MyCdkS3ProjectStack extends cdk.Stack {
    constructor(scope: Construct, id: string, props?: cdk.StackProps);
}
