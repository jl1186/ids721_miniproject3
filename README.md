# AWS S3 Bucket Creation with AWS CDK and CodeWhisperer

This README outlines the steps to create an S3 bucket using the AWS Cloud Development Kit (CDK) with the assistance of AWS CodeWhisperer for code generation.

## Prerequisites

- Node.js and npm (Node Package Manager)
- AWS Command Line Interface (CLI)
- AWS Cloud Development Kit (CDK)

## Project Setup

Initialize a new CDK project in a dedicated directory:

```sh
mkdir my-cdk-s3-project && cd my-cdk-s3-project
cdk init app --language=typescript
```

Install the necessary AWS CDK package for S3:

```sh
npm install @aws-cdk/aws-s3

```

Creating the S3 Bucket using codewhisperer
```sh
// Use AWS CDK to create an S3 bucket with versioning and encryption enabled
```

Compile the TypeScript code into JavaScript:
```sh
npm run build
```

Deploy the CDK stack to your AWS account:
```sh
cdk bootstrap
cdk deploy
```
